## VLC NowPlaying

A VLC track status suitable for panels (eg. Xfce panel via "Generic Monitor" applet)

Preview:

![vlc-nowplaying](./vlc-nowplaying.png)
