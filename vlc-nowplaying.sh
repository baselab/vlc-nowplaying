#!/usr/bin/env bash

data=$(
    /bin/gdbus call \
    --session \
    --dest org.mpris.MediaPlayer2.vlc \
    --object-path /org/mpris/MediaPlayer2 \
    --method org.freedesktop.DBus.Properties.Get \
    org.mpris.MediaPlayer2.Player Metadata \
)

json=$(
    sed -r "
        s/^\(|\)$|objectpath//g ;
        s/< *'|'>|[><]/\"/g ;
        s/, '/, \"/g ;
        s/': /\": /g ;
        s/\{'/{\"/g ;
        s/\"\['/\"/g ;
        s/'\]\"/\"/g ;
        s/^\"|\",$//g ;
    " <<<"${data}" \
    | tr -s '"'
)

echo "${json}" >&2

nowpl=$( jq -r '."vlc:nowplaying"' <<<"${json}" 2>/dev/null | grep -vE "^null$" )
title=$( jq -r '."xesam:title"' <<<"${json}" 2>/dev/null | grep -vE "^null$" )
artist=$( jq -r '."xesam:artist"' <<<"${json}" 2>/dev/null | grep -vE "^null$" )
trackn=$( jq -r '."xesam:tracknumber"' <<<"${json}" 2>/dev/null | grep -vE "^null$" )
filen=$( jq -r '."xesam:url"' <<<"${json}" 2>/dev/null | grep -vE "^null$" | sed -r 's;^file://;;' )

[[ -n "${nowpl}" ]] && { echo "${nowpl}" | sed -r "s/ - /\n> /" ; exit ;}
[[ -n "${artist}" || -n "${title}" || -n "${trackn}" ]] && { echo -e "${artist}\n${trackn}. ${title}" ; exit ;}
[[ -n "${filen}" ]] && { filen="${filen//+/ }"; printf "${filen//%/\\x}" | xargs -d '\n' basename ; exit ;}

echo ""

